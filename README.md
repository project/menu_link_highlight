# Menu Link Highlight

> Adds a highlight-class 'ml-highlight' to a menu-item <li>-elements.

## Installation

1. Install the module the [drupal way](https://www.drupal.org/documentation/install/modules-themes/modules-8)
2. Edit a menu item and check the 'Highlight'-checkbox
3. Grant *Highlight menu links* permission to users which should be able to highlight menu-items.
